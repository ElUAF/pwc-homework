This is implementation of Path finder 

# Local development

## Prerequisities

* installed at least Java SDK 11

## How to run it?

* ```./mvnw clean install```
* ```java -jar target/jstrouhal-0.0.1-SNAPSHOT.jar```
* open in e.g. browser: ```http://localhost:8080/routing/CZE/DEU```
* check swagger: ```http://localhost:8080/swagger-ui/index.html#/routing-controller```

# Architecture

App contains 2 standalone / independent packages and it provides connection (adapters) between them 

* clients
* routing

## Routing

Contains application logic related to routing, including rest controllers.
Routing uses A* algorithm to find path - implemented by library org.jgrapht

## Clients

Contains external connectors and theirs configuration.


# Possible extensions / optimisations 

* There could be added some in memory cache for country data into CountryDataProviderAdapter
  so that we don't need to ask for brand-new data for every single request.
* we can check continents and reject calculation early if there is e.g. origin Europe and target America   

