package com.pwc.homework.jstrouhal

import com.pwc.homework.jstrouhal.clients.CountryClient
import com.pwc.homework.jstrouhal.routing.Country
import com.pwc.homework.jstrouhal.routing.CountryDataProvider
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.stereotype.Component

@SpringBootApplication
class JstrouhalApplication

fun main(args: Array<String>) {
    runApplication<JstrouhalApplication>(*args)
}

/**
 * Adapter for routing package, which adapts client from clients package
 */
@Component
class CountryDataProviderAdapter(
    private val countryClient: CountryClient
) : CountryDataProvider {

    override fun getCountries(): Set<Country> = countryClient
        .getCountries()
        .collectList()
        .block()
        ?.map {
            Country(
                code = it.cca3,
                borders = it.borders,
                latitude = it.getLatitude(),
                longitude = it.getLongitude()
            )
        }
        ?.toSet() ?: setOf()
}
