package com.pwc.homework.jstrouhal.clients

import reactor.core.publisher.Flux

interface CountryClient {

    fun getCountries(): Flux<CountryModel>
}
