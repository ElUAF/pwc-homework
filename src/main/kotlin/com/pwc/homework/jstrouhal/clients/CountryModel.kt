package com.pwc.homework.jstrouhal.clients

import com.fasterxml.jackson.annotation.JsonProperty

data class CountryModel(

    val cca3: String,

    val borders: Set<String>,

    @JsonProperty("latlng")
    val coordinates: List<Double>

) {
    fun getLatitude(): Double? = if (coordinates.isNotEmpty()) {
        coordinates[0]
    } else {
        null
    }


    fun getLongitude(): Double? = if (coordinates.size > 1) {
        coordinates[1]
    } else {
        null
    }
}
