package com.pwc.homework.jstrouhal.clients.rest

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.pwc.homework.jstrouhal.clients.CountryClient
import com.pwc.homework.jstrouhal.clients.CountryModel
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux

@ConfigurationProperties(prefix = "com.pwc.homework.jstrouhal.clients.rest.country-rest-client")
@Configuration
class CountryRestClientConfig {
    lateinit var baseUrl: String
    var maxInMemorySize: Int = 1048576
}

@Component
class CountryRestClient(
    private val objectMapper: ObjectMapper,
    config: CountryRestClientConfig,
) : CountryClient {

    private val client = WebClient.builder()
        .baseUrl(config.baseUrl)
        .codecs { it.defaultCodecs().maxInMemorySize(config.maxInMemorySize) }
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .build()

    override fun getCountries(): Flux<CountryModel> =
        client
            .get()
            .uri("countries.json")
            .accept(MediaType.TEXT_PLAIN)
            .retrieve()
            .bodyToMono(String::class.java)
            .map { objectMapper.readValue<List<CountryModel>>(it) }
            .flatMapIterable { it.asIterable() }

}

