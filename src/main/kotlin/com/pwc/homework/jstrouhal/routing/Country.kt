package com.pwc.homework.jstrouhal.routing

data class Country(
    val code: String,
    val borders: Set<String>,
    val latitude: Double?,
    val longitude: Double?,
)
