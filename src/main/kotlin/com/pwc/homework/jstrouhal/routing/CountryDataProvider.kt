package com.pwc.homework.jstrouhal.routing

interface CountryDataProvider {

    /**
     * Provides data for routing calculation
     */
    fun getCountries(): Set<Country>
}