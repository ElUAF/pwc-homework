package com.pwc.homework.jstrouhal.routing

import org.jgrapht.alg.interfaces.AStarAdmissibleHeuristic
import org.springframework.stereotype.Component

/**
 * Heuristic function calculated as distance between 2 places on the planet Earth.
 */
@Component
class CountryDistanceCalculator: AStarAdmissibleHeuristic<Country> {

    override fun getCostEstimate(from: Country, to: Country): Double {
        if (to.latitude == null || from.latitude == null || to.longitude == null || from.longitude == null) {
            return Double.MAX_VALUE
        }

        val dLat = Math.toRadians(to.latitude - from.latitude)
        val dLon = Math.toRadians(to.longitude - from.longitude)
        val lat1 = Math.toRadians(from.latitude)
        val lat2 = Math.toRadians(to.latitude)

        val a = (Math.pow(Math.sin(dLat / 2), 2.0)
                + Math.pow(Math.sin(dLon / 2), 2.0) * Math.cos(lat1) * Math.cos(lat2))
        val c = 2 * Math.asin(Math.sqrt(a))
        return EARTH_RADIUS_IN_KM * c
    }

    companion object {
        val EARTH_RADIUS_IN_KM = 6371.0
    }
}