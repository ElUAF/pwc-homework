package com.pwc.homework.jstrouhal.routing

import org.jgrapht.Graph
import org.jgrapht.alg.interfaces.AStarAdmissibleHeuristic
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm
import org.jgrapht.alg.shortestpath.AStarShortestPath
import org.jgrapht.graph.DefaultEdge
import org.jgrapht.graph.SimpleGraph
import java.util.stream.Collectors

/**
 * Main calculation class, which uses org.jgrapht internally
 */
class RouteCalculator(
    countries: Set<Country>,
    heuristicAlgorithm: AStarAdmissibleHeuristic<Country>
) {
    private val countriesCache = countries.stream().collect(Collectors.toMap({ it.code }, { it }))
    private val pathAlgorithm = createPath(countries, heuristicAlgorithm)

    fun findRoute(origin: String, destination: String): List<String> {
        val originCountry = countriesCache[origin]
        val destinationCountry = countriesCache[destination]

        if (originCountry != null && destinationCountry != null) {
            return pathAlgorithm
                .getPath(originCountry, destinationCountry)
                ?.vertexList
                ?.map { it.code } ?: listOf()
        } else {
            return listOf()
        }
    }

    private fun createPath(countries: Set<Country>, heuristicAlgorithm: AStarAdmissibleHeuristic<Country>): ShortestPathAlgorithm<Country, DefaultEdge> {
        val g: Graph<Country, DefaultEdge> = SimpleGraph(DefaultEdge::class.java)

        countries.forEach {
            g.addVertex(it)
        }

        countries.forEach {
            it.borders.forEach { border ->
                val target = countriesCache[border]
                if (target != null) {
                    g.addEdge(it, target)
                }
            }
        }

        return AStarShortestPath(g, heuristicAlgorithm)
    }
}