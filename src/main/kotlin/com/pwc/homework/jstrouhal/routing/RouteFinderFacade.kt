package com.pwc.homework.jstrouhal.routing

/**
 * Facade for finding route
 */
interface RouteFinderFacade {

    /**
     * Finds route between origin and destination.
     *
     * @return empty list of route is not possible
     */
    fun findRoute(origin: String, destination: String): List<String>
}