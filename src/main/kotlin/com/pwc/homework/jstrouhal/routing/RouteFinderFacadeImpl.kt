package com.pwc.homework.jstrouhal.routing

import org.jgrapht.alg.interfaces.AStarAdmissibleHeuristic
import org.springframework.stereotype.Component

@Component
class RouteFinderFacadeImpl(
    private val countryDataProvider: CountryDataProvider,
    private val heuristicAlgorithm: AStarAdmissibleHeuristic<Country>
): RouteFinderFacade {

    override fun findRoute(origin: String, destination: String): List<String> = countryDataProvider
        .getCountries()
        .let { RouteCalculator(it, heuristicAlgorithm) }
        .findRoute(origin, destination)
}
