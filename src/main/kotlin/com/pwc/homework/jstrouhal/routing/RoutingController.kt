package com.pwc.homework.jstrouhal.routing

import io.swagger.annotations.*
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("routing/")
class RoutingController(
    private val routeFinderFacade: RouteFinderFacade
) {

    @ApiOperation(value = "Finds the route between origin and destination")
    @ApiResponses(
        ApiResponse(
            code = 200,
            message = "successful operation",
            examples = Example(
                ExampleProperty(mediaType = MediaType.APPLICATION_JSON_VALUE, value = "[\"CZE\", \"POL\"]")
            )
        ),
        ApiResponse(
            code = 400,
            message = "route is not found",
            examples = Example(
                ExampleProperty(mediaType = MediaType.TEXT_PLAIN_VALUE, value = "Error message")
            )
        ),
    )
    @GetMapping("{origin}/{destination}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun findRoute(
        @ApiParam(required = true, example = "CZE", value = "origin country code")
        @PathVariable("origin")
        origin: String,

        @ApiParam(required = true, example = "POL", value = "destination country code")
        @PathVariable("destination")
        destination: String
    ): ResponseEntity<Any> {
        val route = routeFinderFacade.findRoute(origin, destination)

        return if (route.isNotEmpty()) {
            ResponseEntity.ok(route)
        } else {
            ResponseEntity.badRequest().body("Path not found between $origin and $destination")
        }
    }
}
