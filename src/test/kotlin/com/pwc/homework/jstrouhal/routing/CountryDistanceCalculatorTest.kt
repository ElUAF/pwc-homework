package com.pwc.homework.jstrouhal.routing

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class CountryDistanceCalculatorTest {

    private val calculator = CountryDistanceCalculator()

    @ParameterizedTest
    @MethodSource("testData")
    fun testWithData(from: Country, to: Country, expected: Int) {
        val actual = calculator.getCostEstimate(from, to)

        Assertions.assertEquals(expected, actual.toInt())
    }

    companion object {

        const val CZE_DEU_DESTINATION = 481
        const val CZE_ESP_DESTINATION = 1871

        @JvmStatic
        fun testData() = Stream.of(
            arg(
                from = CZE,
                to = DEU,
                expected = CZE_DEU_DESTINATION
            ),
            arg(
                from = CZE,
                to = DEU,
                expected = CZE_DEU_DESTINATION
            ),
            arg(
                from = CZE,
                to = ESP,
                expected = CZE_ESP_DESTINATION
            ),
            arg(
                from = ESP,
                to = CZE,
                expected = CZE_ESP_DESTINATION
            ),
            arg(
                from = UNKNOWN_LATLNG,
                to = CZE,
                expected = Int.MAX_VALUE
            ),
            arg(
                from = CZE,
                to = UNKNOWN_LATLNG,
                expected = Int.MAX_VALUE
            ),
            arg(
                from = UNKNOWN_LATLNG,
                to = UNKNOWN_LATLNG,
                expected = Int.MAX_VALUE
            ),
        )

        private fun arg(
            from: Country, to: Country, expected: Int
        ) = Arguments.of(
            from,
            to,
            expected
        )
    }
}