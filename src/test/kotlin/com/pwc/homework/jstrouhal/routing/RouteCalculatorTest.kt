package com.pwc.homework.jstrouhal.routing

import org.jgrapht.alg.interfaces.AStarAdmissibleHeuristic
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class TestHeuristicAlgorithm : AStarAdmissibleHeuristic<Country> {
    override fun getCostEstimate(p0: Country?, p1: Country?): Double = 0.0
}

class RouteCalculatorTest {

    @ParameterizedTest
    @MethodSource("testData")
    fun testWithData(countries: Set<Country>, origin: String, destination: String, expected: List<String>) {
        val calculator = RouteCalculator(countries, TestHeuristicAlgorithm())
        val actual = calculator.findRoute(origin, destination)

        Assertions.assertEquals(expected, actual)
    }

    companion object {

        @JvmStatic
        fun testData() = Stream.of(
            arg(
                countries = setOf(),
                origin = CZE,
                destination = ESP
            ),
            arg(
                countries = setOf(CZE, DEU),
                origin = CZE,
                destination = DEU,
                expected = listOf(CZE, DEU)
            ),
            arg(
                countries = setOf(CZE, DEU, FRA, ESP),
                origin = CZE,
                destination = ESP,
                expected = listOf(CZE, DEU, FRA, ESP)
            ),
            arg(
                countries = setOf(CZE, USA),
                origin = CZE,
                destination = USA
            ),
        )

        private fun arg(
            countries: Set<Country>,
            origin: Country,
            destination: Country,
            expected: List<Country> = listOf()
        ) = Arguments.of(
            countries,
            origin.code,
            destination.code,
            expected.map { it.code }
        )
    }
}