package com.pwc.homework.jstrouhal.routing

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.reactive.server.WebTestClient
import java.util.stream.Stream


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RoutingControllerIT {

    @Autowired
    private lateinit var webTestClient: WebTestClient

    @ParameterizedTest
    @MethodSource("existingPathData")
    fun testExistingPath(from: Country, to: Country, expected: List<String>) {
        val expectedBody = webTestClient.get()
            .uri("routing/${from.code}/${to.code}")
            .exchange()
            .expectStatus().is2xxSuccessful
            .expectBody()
            .jsonPath("\$.length()").isEqualTo(expected.size)

        (expected.indices).forEach {
            expectedBody.jsonPath("\$[${it}]").isEqualTo(expected[it])
        }
    }

    @ParameterizedTest
    @MethodSource("notExistingPathData")
    fun testNotExistingPath(from: Country, to: Country) {
        val expectedBody = webTestClient.get()
            .uri("routing/${from.code}/${to.code}")
            .exchange()
            .expectStatus().isBadRequest
    }


    companion object {

        @JvmStatic
        fun existingPathData() = Stream.of(
            arg(from = CZE, to = DEU, expected = listOf(CZE, DEU)),
            arg(from = CZE, to = ESP, expected = listOf(CZE, DEU, FRA, ESP))
        )

        @JvmStatic
        fun notExistingPathData() = Stream.of(
            arg(from = CZE, to = USA),
        )

        private fun arg(
            from: Country, to: Country, expected: List<Country> = listOf()
        ) = Arguments.of(
            from,
            to,
            expected.map { it.code }
        )
    }
}