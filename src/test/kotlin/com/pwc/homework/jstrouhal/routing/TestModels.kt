package com.pwc.homework.jstrouhal.routing

val CZE = Country(code = "CZE", borders = setOf("AUT", "DEU", "POL", "SVK"), latitude = 49.75, longitude = 15.5)
val ESP = Country(code = "ESP", borders = setOf("AND", "FRA", "GIB", "PRT", "MAR"), latitude = 40.0, longitude = -4.0)
val FRA = Country(
    code = "FRA",
    borders = setOf("AND", "BEL", "DEU", "ITA", "LUX", "MCO", "ESP", "CHE"),
    latitude = 46.0, longitude = 2.0
)
val DEU = Country(
    code = "DEU",
    borders = setOf("AUT", "BEL", "CZE", "DNK", "FRA", "LUX", "NLD", "POL", "CHE"),
    latitude = 51.0, longitude = 9.0
)

val USA = Country(
    code = "USA",
    borders = setOf("CAN", "MEX"),
    latitude = 38.0, longitude = -97.0
)

val UNKNOWN_LATLNG = Country(
    code = "DEU",
    borders = setOf("AUT", "BEL", "CZE", "DNK", "FRA", "LUX", "NLD", "POL", "CHE"),
    latitude = null,
    longitude = null,
)